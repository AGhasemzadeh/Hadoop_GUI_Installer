#-------------------------------------------------
#
# Project created by QtCreator 2016-08-04T06:48:01
#
#-------------------------------------------------

QT       += core gui
QT        += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hadoop_installer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
