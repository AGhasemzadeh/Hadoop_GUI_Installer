#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
#include <QFile>
#include <QProcess>
#include <QDebug>
#include <QString>
#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>
#include <QListWidgetItem>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    model = new QStringListModel(this);
    QStringList List;

    QFile inputFile(QDir::currentPath() + "/node_list");
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          List << line;
       }
       inputFile.close();
    }
    model->setStringList(List);
    ui->nodeList->setModel(model);
    ui->nodeList->setEditTriggers(QAbstractItemView::AnyKeyPressed | QAbstractItemView::DoubleClicked);
}

MainWindow::~MainWindow()
{
    delete ui;
}
bool copy_dir_recursive(QString from_dir, QString to_dir, bool replace_on_conflit)
{
    QDir dir;
    dir.setPath(from_dir);

    from_dir += QDir::separator();
    to_dir += QDir::separator();

    foreach (QString copy_file, dir.entryList(QDir::Files))
    {
        QString from = from_dir + copy_file;
        QString to = to_dir + copy_file;

        if (QFile::exists(to))
        {
            if (replace_on_conflit)
            {
                if (QFile::remove(to) == false)
                {
                    return false;
                }
            }
            else
            {
                continue;
            }
        }

        if (QFile::copy(from, to) == false)
        {
            return false;
        }
    }

    foreach (QString copy_dir, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
    {
        QString from = from_dir + copy_dir;
        QString to = to_dir + copy_dir;

        if (dir.mkpath(to) == false)
        {
            return false;
        }

        if (copy_dir_recursive(from, to, replace_on_conflit) == false)
        {
            return false;
        }
    }

    return true;
}

void MainWindow::on_enableRoot_clicked()
{
    //Enable Root
    QFile file( QDir::currentPath() + "/enable_root.sh" );
    if( !file.exists() )
    {
      qDebug() << "The file" << file.fileName() << "does not exist.";
    } else {
        QString user_password = ui->passwordText->text();
        QString line1 = "echo " + user_password + " | sudo -S passwd -d root";
        QFile f(QDir::currentPath() + "/enable_root.sh");
        if(f.open(QFile::WriteOnly))
        {
            QTextStream stream(&f);
            stream << line1 << endl;
            stream << "sudo su <<HERE" << endl;
            stream << "echo 'greeter-show-manual-login=true' >> /usr/share/lightdm/lightdm.conf.d/50-unity-greeter.conf" << endl;
            stream << "HERE";
            f.close();
        }
    }

    QProcess *process = new QProcess(this);
    QString file2 = QDir::currentPath() + "/enable_root.sh";
    process->start("bash",QStringList() << file2);

    if(process->waitForStarted() == false)
    {
        qDebug() << "Error starting external program";
    }

    process->waitForReadyRead(100);
    process->waitForFinished(1000);
    qDebug() << "read output" << process->readAllStandardOutput();
       QMessageBox::information(
                this,
                tr("Hadoop GUI"),
                tr("Please reset computer.") );

}

void MainWindow::on_resetForRoot_clicked()
{
    //Reset System
    QProcess process;
    process.startDetached("shutdown -r now");
    process.waitForStarted();
    process.waitForFinished();
    QMessageBox::information(
            this,
            tr("Hadoop GUI"),
            tr("Wait to reset.") );

}

void MainWindow::on_sshServerInstall_clicked()
{
    //Server SSH
    QProcess *process = new QProcess(this);
    process->start("sudo apt-get -y --force-yes install openssh-server");
    if(process->waitForStarted() == false)
    {
        qDebug() << "Error starting external program";
    }
    process->waitForFinished();
    qDebug() << "read output" << process->readAllStandardOutput();
       QMessageBox::information(
                this,
                tr("Hadoop GUI"),
                tr("OpenSSH Server Installed.") );
}

void MainWindow::on_sshClientInstall_clicked()
{
    //Client SSH
    QProcess *process = new QProcess(this);
    process->start("sudo apt-get -y --force-yes install openssh-client");
    if(process->waitForStarted() == false)
    {
        qDebug() << "Error starting external program";
    }
    process->waitForFinished();
    qDebug() << "read output" << process->readAllStandardOutput();
       QMessageBox::information(
                this,
                tr("Hadoop GUI"),
                tr("OpenSSH Client Installed.") );
}

void MainWindow::on_downloadJDK_clicked()
{
    //Download JDK
    QString link = "http://ftp.osuosl.org/pub/funtoo/distfiles/oracle-java";
    QDesktopServices::openUrl(QUrl(link));
    QMessageBox::information(
             this,
             tr("Hadoop GUI"),
             tr("After download please extract and copy file to excute directory with named jdk(Note bin directory should be in jdk folder)") );
}

void MainWindow::on_installJDK_clicked()
{
    //Install JDK
    QDir().mkdir("/usr/local/java");
    copy_dir_recursive(QDir::currentPath() + "/jdk", "/usr/local/java",true);
    QFile file("/etc/profile");
    if (file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        QTextStream stream(&file);
        stream << endl;
        stream << "export JAVA_HOME=/usr/local/java" << endl;
        stream << "export JRE_HOME=$JAVA_HOME/jre" << endl;
        stream << "export CLASSPATH=.:$JAVA_HOME/lib:$JRE_HOME/lib:$CLASSPATH" << endl;
        stream << "export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH" << endl;
        file.close();
    }
    QMessageBox::information(
             this,
             tr("Hadoop GUI"),
             tr("JDK installed.") );
}

void MainWindow::on_downloadHadoop_clicked()
{
    //Download Hadoop
    QString link = "https://archive.apache.org/dist/hadoop/core/";
    QDesktopServices::openUrl(QUrl(link));
    QMessageBox::information(
             this,
             tr("Hadoop GUI"),
             tr("After download please extract and copy file to excute directory with named hadoop(Note bin directory should be in hadoop folder)") );
}

void MainWindow::on_installHadoop_clicked()
{
    //Install Hadoop
    QDir().mkdir("/usr/local/hadoop");
    QDir().mkdir("/home/hadoop");
    QDir().mkdir("/home/hadoop/data");
    QDir().mkdir("/home/hadoop/name");
    copy_dir_recursive(QDir::currentPath() + "/hadoop", "/usr/local/hadoop",true);
    copy_dir_recursive(QDir::currentPath() + "/hadoop_config", "/usr/local/hadoop/etc/hadoop",true);
    QFile file("/etc/profile");
    if (file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        QTextStream stream(&file);
        stream << endl;
        stream << "export HADOOP_HOME=/usr/local/hadoop" << endl;
        stream << "export PATH=$HADOOP_HOME/bin:$PATH" << endl;
        stream << "export PATH=$HADOOP_HOME/sbin:$PATH" << endl;
        stream << "export HADOOP_MAPRED_HOME=$HADOOP_HOME" << endl;
        stream << "export HADOOP_COMMON_HOME=$HADOOP_HOME" << endl;
        stream << "export HADOOP_HDFS_HOME=$HADOOP_HOME" << endl;
        stream << "export YARN_HOME=$HADOOP_HOME" << endl;
        stream << "export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native" << endl;
        stream << "export HADOOP_OPTS=\"-Djava.library.path=$HADOOP_HOME/lib\"" << endl;
        file.close();
    }
    QMessageBox::information(
             this,
             tr("Hadoop GUI"),
             tr("Hadoop installed.") );
}

void MainWindow::on_resetComputer_clicked()
{
    QProcess process;
    process.startDetached("shutdown -r now");
    process.waitForStarted();
    process.waitForFinished();
    QMessageBox::information(
            this,
            tr("Hadoop GUI"),
            tr("Wait to reset.") );
}

void MainWindow::on_addItem_clicked()
{
    //Add Item
    int row = model->rowCount();

    // Enable add one or more rows
    model->insertRows(row,1);

    // Get the row for Edit mode
    QModelIndex index = model->index(row);

    // Enable item selection and put it edit mode
    ui->nodeList->setCurrentIndex(index);
    ui->nodeList->edit(index);
}

void MainWindow::on_removeItem_clicked()
{
    //Remove Item
    model->removeRows(ui->nodeList->currentIndex().row(),1);
}

void MainWindow::on_setConfig_clicked()
{
    //Config IPs & slaves
    QFile nodeListFile(QDir::currentPath() + "/node_list");
    if (nodeListFile.open(QFile::WriteOnly))
    {
        QTextStream stream(&nodeListFile);
        int count = model->rowCount();
        for(int index = 0;index < count;index++)
        {
            QString itemText = model->index( index, 0 ).data( Qt::DisplayRole ).toString();
            stream << itemText << endl;
        }

        nodeListFile.close();
    }
    //Go to Hadoop in hosts file
    int flag = 0;
    QFile hostFile("/etc/hosts");
    if (hostFile.open(QIODevice::ReadWrite))
    {
       int flag_remove = 0;
       QTextStream in(&hostFile);
       while (!in.atEnd())
       {
          QString s;
          QString line = in.readLine();
          if(line == "#Hadoop")
          {
              flag_remove = 1;
              //in << "#Hadoop" << endl;
              int count = model->rowCount();
              for(int index = 0;index < count;index++)
              {
                  QString itemText = model->index( index, 0 ).data( Qt::DisplayRole ).toString();
                  QStringList nodeInfo = itemText.split(",");
                  in << nodeInfo[1] << " " << nodeInfo[0] << endl;
              }
          } else {
              flag = 1;
          }
          if(flag_remove == 1)
          {

          }
       }
       hostFile.close();
    }
    if(flag == 1)
    {
        QFile hostFileNew("/etc/hosts");
        if(hostFileNew.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            QTextStream in(&hostFileNew);
            in << "#Hadoop" << endl;
            int count = model->rowCount();
            for(int index = 0;index < count;index++)
            {
                QString itemText = model->index( index, 0 ).data( Qt::DisplayRole ).toString();
                QStringList nodeInfo = itemText.split(",");
                in << nodeInfo[1] << " " << nodeInfo[0] << endl;
            }
            hostFileNew.close();
        }
    }
    //Make slaves
    QFile nodeListFileS("/usr/local/hadoop/etc/hadoop/slaves");
    if (nodeListFileS.open(QFile::WriteOnly))
    {
        QTextStream stream(&nodeListFileS);
        int count = model->rowCount();
        for(int index = 0;index < count;index++)
        {
            QString itemText = model->index( index, 0 ).data( Qt::DisplayRole ).toString();
            QStringList nodeInfo = itemText.split(",");
            stream << nodeInfo[0] << endl;
        }

        nodeListFileS.close();
    } else {
        QMessageBox::information(
                this,
                tr("Hadoop GUI"),
                tr("Error!") );
    }
    QMessageBox::information(
            this,
            tr("Hadoop GUI"),
            tr("Hosts and Slaves configuration done.") );
}

void MainWindow::on_publicKeyGen_clicked()
{
    //Public key generation and exchange
    QMessageBox::information(
            this,
            tr("Hadoop GUI"),
            tr("Please run ssh-keygen and share public keys with nodes.") );
}
